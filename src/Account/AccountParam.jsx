import React from 'react'
import { useParams } from 'react-router'

function AccountParam() {
    let para = useParams();
    return (
        <div>
            <h4>Name: {para.name}</h4>
        </div>
    )
}

export default AccountParam
