import React from 'react'
import {Button} from 'react-bootstrap'
import {Link, useLocation} from 'react-router-dom';
import Categories from './Categories';
function Movie() {
    const category = [
        {
            name: "Action",
            query:"action"
        },
        {
            name: "Crime",
            query:"crime"
        },
        {
            name: "Romance",
            query:"romance"
        },
        {
            name: "Comedy",
            query:"comedy"
        },
        {
            name: "Superpower",
            query:"superpower"
        },
        {
            name: "Adventure",
            query:"adventure"
        },
    ]
    function useQuery (){
        return new URLSearchParams(useLocation().search);
    }
    let find = useQuery();
    return (
        <>
            <h2>Movie Category</h2>
            {category.map((obj,i)=><Button as={Link} to={`/video/movie?type=${obj.query}`} variant="secondary">{obj.name}</Button>
            )}
            <Categories title={find.get("type")}/>
        </>
    )
}

export default Movie
