import React from 'react'

function Categories({title}) {
    return (
        <div>
            Category: {title}
        </div>
    )
}

export default Categories
