import React from 'react'
import {Button, Container} from 'react-bootstrap'
function Welcome(props) {
    return (
        <div>
            <Container>
                <h3>Welcome</h3>
                <Button onClick={props.onSign}>{props.isSingIn ? "Sing out" : "Log in"}</Button>
            </Container>
            
        </div>
    )
}

export default Welcome
