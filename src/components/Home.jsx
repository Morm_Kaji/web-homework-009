import React from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


function Home() {
    const data = [
        {
            id: 1,
            titl: "Seven Deadly Sins",
            Content: "contents",
            img:"https://www.pngkit.com/png/detail/337-3373445_download-seven-deadly-sins-logo.png"
        },
        {
            id: 2,
            titl: "Angel Beats",
            Content: "contents",
            img: "https://www.pngkit.com/png/detail/75-752862_angel-beats-png-logo-angel-beats.png"
        },
        {
            id: 3,
            titl: "Goblin Slayer",
            Content: "contents",
            img: "https://mangathrill.com/wp-content/uploads/2019/05/goblinnn-784x420.png"
        },
        {
            id: 4,
            titl: "Fate Stay Night",
            Content: "contents",
            img: "https://www.pngkey.com/png/detail/381-3813095_fate-stay-night-unlimited-blade-works-logo-ideas.png"
        },
        {
            id: 5,
            titl: "Tenggen Toppa Gurren Lagann",
            Content: "contents",
            img: "https://i.pinimg.com/564x/f2/50/2d/f2502ddf03e6b734ef2e87108a3cb6c6.jpg"
        },
        {
            id: 6,
            titl: "Berserk",
            Content: "contents",
            img: "https://m.media-amazon.com/images/S/aplus-media/vc/a5571e5c-334b-43be-9d77-097a305db53f.__CR0,24,970,300_PT0_SX970_V1___.jpg"
        },
        {
            id: 7,
            titl: "Log Horizon",
            Content: "contents",
            img: "https://technoinfoplus.com/wp-content/uploads/2020/05/download-2020-05-05T041358.055.jpg"
        },
    ]


    return (
        <div>
            <div className="container">
                <Row>
                    {data.map((obj, i) => (
                        <>
                            <Col md="3" key={i} className=" my-4 ">
                                <Card  width="200">
                                    <Card.Img variant="top"  height="100" src={obj.img} />
                                    <Card.Body>
                                        <Card.Title>{obj.titl}</Card.Title>
                                        <Card.Text>
                                            {obj.Content}
                                        </Card.Text>
                                    </Card.Body>
                                    <Button as={Link} to={`/homeparam/${obj.titl}`} variant="secondary" >Read More</Button>
                                </Card>
                            </Col>
                        </>
                    ))}
                </Row>
            </div>

        </div >
    )
}

export default Home
