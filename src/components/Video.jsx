import React from 'react'
import { Button, Container } from 'react-bootstrap'
import { Link, Route, Switch, useRouteMatch} from 'react-router-dom';
import Animation from '../videoPages/Animation';
import Movie from '../videoPages/Movie';


function Video() {
    let {url } = useRouteMatch();
    return (
        <Container>
            <div>
                <h2>Video</h2>
                <Button as={Link} variant="secondary" to={`${url}/movie`}>Moive</Button>
                <Button as={Link} variant="secondary" to={`${url}/animation`}>Animation</Button>
                <Switch>
                    <Route path="/video/movie" render={() => <Movie />} />
                    <Route path="/video/animation" render={() => <Animation />} />
                </Switch>
            </div>
        </Container>
    )
}

export default Video
