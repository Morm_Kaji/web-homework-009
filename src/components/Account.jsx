import React from 'react'
import { Container } from 'react-bootstrap';
import { Switch, Route, Link } from 'react-router-dom';
import AccountParam from '../Account/AccountParam';
function Account() {
    const data = [
        {
            name: "Netflix"
        },
        {
            name: "Zillow Group"
        },
        {
            name: "Yahoo"
        },
        {
            name: "Modus Create"
        },
    ]
    return (
        <>
            <Container>
                {data.map((obj, i) =>
                (<ul key={i}>
                    <li>
                        <Link to={`/account/${obj.name}`}>{obj.name}</Link>
                    </li>
                </ul>
                ))}
                <Switch>
                    <Route path="/account/:name" render={() => <AccountParam />} />
                </Switch>

            </Container>
        </>
    )
}

export default Account
