import React from 'react'
import { Navbar, Form, Nav, FormControl, Button } from 'react-bootstrap';
import { Link} from 'react-router-dom';


function NavFrom() {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Link to="/"><Navbar.Brand href="#">React-Router</Navbar.Brand></Link>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="mr-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to="/video">Video</Nav.Link>
                        <Nav.Link as={Link} to="/account">Account</Nav.Link>
                        <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
                        <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="mr-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>

        </div>
    )
}

export default NavFrom
