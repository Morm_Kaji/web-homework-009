import { Redirect } from "react-router"
import React from 'react'

function ProtectedRoute({isSingIs, render: Component, pathURL}) {
    if(isSingIs){
        return <Component path={pathURL}/>
    }else{
        return <Redirect to="/auth"/>
    }
}

export default ProtectedRoute
