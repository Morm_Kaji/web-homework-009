import React from 'react'
import { Container } from 'react-bootstrap';
import { useParams } from 'react-router'

function HomeParam() {
    let param = useParams();
    return (
        <div>
            <Container>
                <h1>Title: {param.titl}</h1>
            </Container>
        </div>
    )
}

export default HomeParam
