import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import React, {useState} from 'react'
import NavFrom from './components/NavFrom';
import Home from './components/Home';
import Video from './components/Video';
import Account from './components/Account';
import Welcome from './components/Welcome';
import Auth from './components/Auth';
import HomeParam from './HomeParam/HomeParam';
import ProtectedRoute from './components/ProtectedRoute';

function App() {
  const [isSingIn, setSignIn] = useState(false);
    function onSign(){
        setSignIn(!isSingIn);
    }
  return (
    <div className="App">
      
      <Router>
      <NavFrom/>
        <Switch>
          <Route exact path="/" render={()=><Home/>}/>
          <Route  path="/video" render={()=><Video/>}/>
          <Route  path="/account" render={()=><Account />}/>
          <Route  path="/auth" render={()=><Auth onSign={onSign} isSingIn={isSingIn}/>}/>
          <Route  path="/homeparam/:titl" render={()=><HomeParam/>}/>
          <ProtectedRoute isSingIs={isSingIn} render={()=><Welcome onSign={onSign} isSingIn={isSingIn}/>} path="/welcome" />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
